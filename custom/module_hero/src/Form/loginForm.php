<?php

namespace Drupal\module_hero\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;


/**
 * Our custom ajax form.
 */
class loginForm extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_ajaxhero";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Seu e-mail *'),
    ];

    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Digite uma senha'),
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Entrar'),
      '#button_type' => 'primary',
    ];

     return $form;

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /**
     * Campos de criação de atributos do novo usuário
     */
    $email =  $form_state->getValues()['email'];
 
    /**
     * Iniciando variáveis
     */
    $count_errors = 0;
    $msg_erro = "";
    $msg_erroa = "";
    $msg_errob = "";
    $get_attributes = "";
    $user_existe = 0;


    /**
     * Inicio validação dos campos do formulário de criação de usuários
     */


    if($form_state->getValues()['email'] == "" ||  $form_state->getValues()['senha1'] == ""){
      $count_errors += 1;
      $msg_erro = "Você deve preencher os campos de usuario e senha.";
    }
  

    /**
     * Fim do formulário de criação de usuários.
     */
  
    if($count_errors > 0){
      \Drupal::messenger()->addError("Erro: ".$msg_erro);
    }else{

        try {

          /**
           * Body do XML de criação de usuário.
           */

          $data = [
            'username' => $form_state->getValues()['email'],
            'password' => $form_state->getValues()['senha1'],
          ];

          /**
           * Headers do XML do envelope
           */

          $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
              'trace' => 1,
          ));
          
          // SET HEADERS
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          
          // Check if service is available
          $serviceStatus = $client->IsServiceAvailable();

          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }


          /**
           * Criação de usuários
           */
          $res = $client->ValidateUser($data);
          $get_attributes = $res->ValidateUserResult;

          $chk_status_login = 0;
          
          switch ($get_attributes->Status) {
            case 'InvalidLogin':
              \Drupal::messenger()->addError("Erro no login!");
              $chk_status_login = 1;
            case 'Unconfirmed':

                try {
                  $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                      'trace' => 1,
                  ));
                  // SET HEADERS
                  $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                  $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                  $client->__setSoapHeaders($header);
                  
                  // Check if service is available
                  $serviceStatus = $client->IsServiceAvailable();
                                  
                  $res_attr = $client->GetAttributes(['userName' => $email]);
                  $attributes = json_encode($res_attr->GetAttributesResult->Atributo);
                  $x = json_decode($attributes);

                  $attr = [
                    "cod_atributo_21" => $x[0]->CodigoAtributo,
                    "nome_atributo_21" => $x[0]->NomeAtributo,
                    "valor_multivalorado_21" => $x[0]->ValorMultivalorado,
                    "cod_atributo_606" => $x[1]->CodigoAtributo,
                    "nome_atributo_606" => $x[1]->NomeAtributo,
                    "valor_multivalorado_606" => $x[1]->ValorMultivalorado,
                    "cod_atributo_607" => $x[2]->CodigoAtributo,
                    "nome_atributo_607" => $x[2]->NomeAtributo,
                    "valor_multivalorado_607" => $x[2]->ValorMultivalorado,
                    "cod_atributo_613" => $x[3]->CodigoAtributo,
                    "nome_atributo_613" => $x[3]->NomeAtributo,
                    "valor_multivalorado_613" => $x[3]->ValorMultivalorado,
                    "cod_atributo_9" => $x[4]->CodigoAtributo,
                    "nome_atributo_9" => $x[4]->NomeAtributo,
                    "valor_multivalorado_9" => $x[4]->ValorMultivalorado,
                    "cod_atributo_13" => $x[5]->CodigoAtributo,
                    "nome_atributo_13" => $x[5]->NomeAtributo,
                    "valor_multivalorado_13" => $x[5]->ValorMultivalorado
                  ];

                  $_SESSION["get_attribute"] = $attr;
      
              } catch (SoapFault $exception) {
                  echo '<pre>';
                  echo $exception->getMessage();
                  echo "\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
              }

              try {
                $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                    'trace' => 1,
                ));
                // SET HEADERS
                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                $client->__setSoapHeaders($header);
                
                // Check if service is available
                $serviceStatus = $client->IsServiceAvailable();
                
                $res = $client->GetUser(['username' => $email]);

                $attr_user = [
                  "ativo" => $res->GetUserResult->Ativo,
                  "cpf" => $res->GetUserResult->CPF,
                  "cpf_responsavel" => $res->GetUserResult->CPFResponsavel,
                  "codigo" => $res->GetUserResult->Codigo,
                  "codigo_area_site_criacao" => $res->GetUserResult->CodigoAreaSiteCriacao,
                  "confirmacao_email" => $res->GetUserResult->ConfirmacaoEmail,
                  "data_acesso_bloquado" => $res->GetUserResult->DataAcessoBloqueado,
                  "data_alteracao" => $res->GetUserResult->DataAlteracao,
                  "data_criacao" => $res->GetUserResult->DataCriacao,
                  "data_nascimento" => $res->GetUserResult->DataNascimento,
                  "data_ultimo_login" => $res->GetUserResult->DataUltimoLogin,
                  "email" => $res->GetUserResult->Email,
                  "email_responsavel" => $res->GetUserResult->EmailResponsavel,
                  "expiration_date_token" => $res->GetUserResult->ExpirationDateToken,
                  "forgot_password_token" => $res->GetUserResult->ForgotPasswordToken,
                  "ip_criacao" => $res->GetUserResult->IPCriacao,
                  "login_result" => $res->GetUserResult->LoginResult,
                  "match_code_id" => $res->GetUserResult->MatchCodeId,
                  "nome" => $res->GetUserResult->Nome,
                  "nome_responsavel" => $res->GetUserResult->NomeResponsavel,
                  "quantidade_de_falhas_login" => $res->GetUserResult->QuantidadeDeFalhasLogin,
                  "questao_seguranca" => $res->GetUserResult->QuestaoSeguranca,
                  "resposta_questao_seguranca" => $res->GetUserResult->RespostaQuestaoSeguranca,
                  "senha" => $res->GetUserResult->Senha,
                  "tipo_cadastro" => $res->GetUserResult->Tipo_Cadastro,
                  "user_social_id" => $res->GetUserResult->UserSocialId

                ];

                $_SESSION["get_user"] = $attr_user;
    

                if($chk_status_login == 0){
                  $user = user_load_by_mail($form_state->getValues()['email']);
                
                  if($user){
                    $uid = $user->id();
                    $user = \Drupal\user\Entity\User::load($uid);
                    \Drupal::messenger()->addMessage("Login efetuado!");
                    user_login_finalize($user);
                    $redirect_path = "/node/1";
                    $url = url::fromUserInput($redirect_path);
                    $form_state->setRedirectUrl($url);
                  }else{
                      /**
                       * Cria usuário no Drupal
                       */

                      $nm_user_drupal = $form_state->getValue('nome');
                      $nm_array = explode(" ", $nm_user_drupal);
                      $randon_user = bin2hex(openssl_random_pseudo_bytes(8));
                      $nm_user_drupal = strtolower($nm_array[0]).$randon_user;

                      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
                      $user = \Drupal\user\Entity\User::create();
                      $user->setPassword($form_state->getValue('senha1'));
                      $user->enforceIsNew();
                      $user->setEmail($email);
                      $user->setUsername($nm_user_drupal);//This username must be unique and accept only a-Z,0-9, - _ @ .
                      $user->set("init", 'email');
                      $user->set("langcode", $language);
                      $user->set("preferred_langcode", $language);
                      $user->set("preferred_admin_langcode", $language);
                      $user->activate();

                      //Save user account
                      $user->save();

                      // No email verification required; log in user immediately.
                      _user_mail_notify('register_no_approval_required', $user);
                      user_login_finalize($user);
                      // $form_state->setRedirectUrl($url);
                      // set relative internal path
                      $redirect_path = "/node/1";
                      $url = url::fromUserInput($redirect_path);

                      // set redirect
                      $form_state->setRedirectUrl($url);
      
                  }
      
                }

            } catch (SoapFault $exception) {
                echo '<pre>';
                echo $exception->getMessage();
                echo "\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
            }
          }      

        } catch (SoapFault $exception) {
          $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          \Drupal::messenger()->addError($response);
          \Drupal::messenger()->addError($get_attributes);
        }
    }  
    return "Ok";
  }
}
