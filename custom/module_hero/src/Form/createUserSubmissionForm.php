<?php

namespace Drupal\module_hero\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class createUserSubmissionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_ajaxhero";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Seu e-mail *'),
    ];

    $form['nome'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu nome completo *'),
    ];

    $form['telefone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu número de celular'),
    ];

    $form['datanascimento'] = array(
      '#type' => 'date',
      '#title' => $this
      ->t('Sua data de nascimento *'),
    );

    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Digite uma senha'),
    ];

    $form['senha2'] = [
      '#type' => 'password',
      '#title' => $this->t('Repita aqui a senha criada'),
    ];

    $form['politica'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Concordo que a Nestlé use meus dados de contato e interações para me mandar comunicações de marketing através do programa Com Você. Meu consentimento para este fim é voluntário e posso retirá-lo a qualquer momento. Mais informações disponíveis na <a class="link_black">Política de Privacidade da Nestlé</a>.'),
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('CRIAR AGORA'),
      '#button_type' => 'primary',
    ];

     return $form;

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /**
     * Campos de criação de atributos do novo usuário
     */
    $email =  $form_state->getValues()['email'];
    $telefone = $form_state->getValues()['telefone'];
    $datanascimento = $form_state->getValues()['datanascimento'];

    /**
     * Iniciando variáveis
     */
    $count_errors = 0;
    $msg_erro = "";
    $msg_erroa = "";
    $msg_errob = "";
    $get_attributes = "";
    $user_existe = 0;


    /**
     * Inicio validação dos campos do formulário de criação de usuários
     */

    if($form_state->getValues()['politica'] == 0){
      $count_errors += 1;
      $msg_erroa = "Erro: Você deve marcar a opção de políticas...";
    }
    
    if($form_state->getValues()['senha1'] != $form_state->getValues()['senha2']){
      $count_errors += 1;
      $msg_erro = "As senhas não coincidem \n";
    }else{
      $count_senha = 0;
      if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $form_state->getValues()['senha1']) )
        {
          $count_senha += 1;
        }
        
        if (preg_match("/^\d+$/", $form_state->getValues()['senha1'])) {
          $count_senha += 1;
        } 

        if(preg_match('/[A-Z]/', $form_state->getValues()['senha1'])){
            $count_senha += 1;
         }

         if(preg_match('/[a-z]/',  $form_state->getValues()['senha1'])){
            $count_senha += 1;
         }

         if($count_senha < 2){
          $count_errors += 1;
          $msg_erro = "Erro: A senha deve número, letras maiúsculas e minusculas e ter entre 6 a 10 caracteres.";
         }
    }

    if($form_state->getValues()['nome'] == "" || $form_state->getValues()['email'] == ""){
      $count_errors += 1;
      $msg_errob = "Preencha todos os campos obrigatórios! \n";
    }

    /**
     * Fim do formulário de criação de usuários.
     */

    
    if($count_errors > 0){
      \Drupal::messenger()->addError("Erro: ".$msg_erro);
      \Drupal::messenger()->addError("Erro: ".$msg_erroa);
      \Drupal::messenger()->addError("Erro: ".$msg_errob);

    }else{

        try {

          /**
           * Body do XML de criação de usuário.
           */

          // $datanascimento = $form_state->getValues()['datanascimento'];

          $data = [
            'nome' => $form_state->getValues()['nome'],
            'email' => $form_state->getValues()['email'],
            'dataNascimento' => $datanascimento,
            'dataCriacao' => date('Y-m-d'),
            'senha' => $form_state->getValues()['senha1'],
          ];


          /**
           * Headers do XML do envelope
           */

          $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
              'trace' => 1,
          ));
          
          // SET HEADERS
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          
          // Check if service is available
          $serviceStatus = $client->IsServiceAvailable();

          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }


          /**
           * Criação de usuários
           */

          $res = $client->CreateUser($data);
          $get_attributes =  $res->CreateUserResult;

          /**
           * Testa se já existe usuario com mesmo email.
           */

          if(json_encode($get_attributes) != "{}"){
            if($get_attributes->ValidationError->DescricaoErroCodigo == "6cdd91fc"){
              \Drupal::messenger()->addError("O e-mail já existe!");
            }

            if($get_attributes->ValidationError->NomeAtributo == "IdadeMinima"){
              \Drupal::messenger()->addError("O novo usuário deverá ser maior de 18 anos.");
            }
          }else{
              try {

                $user_infos = $client->GetUser(['username' => $email],);
                $res_user_info = $user_infos->GetUserResult->Codigo;
                
                $data_atributos['atributos'] = [
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'nu_celular',
                    'Valor' => $telefone,
                    'Items' => true
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'fg_politica_privacidade',
                    'Valor' => 'true',
                    'Items' => true
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter_email',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter_sms',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter_telefone',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ]
                ];
                
                /**
                 * Cria atributos do usuário
                 */
                $res_atributes = $client->SaveAttributes($data_atributos);


              /**
               * Cria usuário no Drupal
               */
                
                // Pega o primeiro nome
                //Atribui um hexadecimal ao nome de usuário
                $nm_user_drupal = $form_state->getValue('nome');
                $nm_array = explode(" ", $nm_user_drupal);
                $randon_user = bin2hex(openssl_random_pseudo_bytes(8));
                $nm_user_drupal = strtolower($nm_array[0]).$randon_user;


                $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
                $user = \Drupal\user\Entity\User::create();
                $user->setPassword($form_state->getValue('senha1'));
                $user->enforceIsNew();
                $user->setEmail($email);
                $user->setUsername($nm_user_drupal);//This username must be unique and accept only a-Z,0-9, - _ @ .
                $user->set("init", 'email');
                $user->set("langcode", $language);
                $user->set("preferred_langcode", $language);
                $user->set("preferred_admin_langcode", $language);
                $user->activate();

                //Save user account
                $user->save();

                // No email verification required; log in user immediately.
                _user_mail_notify('register_no_approval_required', $user);
                user_login_finalize($user);
                // $form_state->setRedirectUrl($url);
                // set relative internal path
                $redirect_path = "/node/1";
                $url = url::fromUserInput($redirect_path);

                // set redirect
                $form_state->setRedirectUrl($url);
      
      
                
            } catch (SoapFault $exception) {
              $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
              \Drupal::messenger()->addError($response);
            }
          }

        } catch (SoapFault $exception) {
          $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          \Drupal::messenger()->addError($response);

          \Drupal::messenger()->addError($get_attributes);
        }

    }  

    return "Ok";

  }
}
