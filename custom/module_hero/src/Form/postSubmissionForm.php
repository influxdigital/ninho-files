<?php

namespace Drupal\module_hero\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Our example Form.
 */
class postSubmissionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_exampleform";
   }

   /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

      $form['nome'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome'),
      ];

    $form['email'] = [
    '#type' => 'textfield',
    '#title' => $this->t('E-mail'),
    ];

    $form['CPF'] = [
    '#type' => 'textfield',
    '#title' => $this->t('CPF'),
    ];

    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Digite sua senha'),
      ];

      $form['senha2'] = [
        '#type' => 'password',
        '#title' => $this->t('Confirmar senha'),
        ];

      $form['telefone'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Telefone'),
        ];

    $form['dataNascimento'] = array(
        '#type' => 'date',
        '#title' => $this
          ->t('Data de nascimento'),
        '#default_value' => array(
          'year' => 2021,
          'month' => 2,
          'day' => 12,
        ),
      );


      $form['submit'] = [
        '#type' => 'button',
        '#value' => $this->t('Enviar'),
        
      ];

      return $form;
    }

    /**
     * {@inheritdoc}
     */
     public function submitForm(array &$form, FormStateInterface $form_state) {
       drupal_set_message('Submitting our form...');
     }
}
