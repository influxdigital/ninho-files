<?php

namespace Drupal\module_hero\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class meuCadastroEndereco extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_ajaxhero";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Meu campos
     */

    $form['genero_options'] = array(
      '#type' => 'value',
      '#value' => array('0' => t('Selecionar'),
                        'F' => t('Feminino'),
                        'M' => t('Masculino'))
    );

    $form['genero'] = [
        '#type' => 'select',
        '#title' => $this
          ->t('Seu gênero'),
        '#options' => $form['genero_options']['#value'],
      ];



      $form['endereco'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Seu endereço'),
      ];

      $form['cep'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Seu CEP'),
      ];
    
      $form['message'] = [
        '#type' => 'markup',
        '#markup' => "<div><p>Não sabe o CEP? <a target='_blank' href='http://www.correios.com.br'>Descubra aqui</a> <span class='line'></span></p></div>"
      ];

      $form['estado'] = [
        '#type' => 'select',
        '#title' => $this
          ->t('Estado'),
        '#options' => [
            '0' => $this
            ->t('Selecionar'),
            '1' => $this
            ->t('ACRE'),
            '3' => $this
            ->t('RIO DE JANEIRO'),
            '4' => $this
            ->t('RIO GRANDE DO SUL'),
            '5' => $this
            ->t('SÃO PAULO')
        ],
      ];

      $form['cidade'] = [
        '#type' => 'select',
        '#title' => $this
            ->t('Cidade'),
            '#options' => [
            '0' => $this
            ->t('Rio da Janeiro'),
            '1' => $this
            ->t('Rio Grande do Sul'),
            '2' => $this
            ->t('Salvador'),
            '3' => $this
            ->t('São Paulo'),
            '4' => $this
            ->t('Porto Alegre')
        ],
      ];


      $form['bairro'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Bairro'),
        '#placeholder' => $this->t('digite aqui')
      ];


      $form['numero'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Número'),
        '#placeholder' => $this->t('digite aqui')
      ];


      $form['complemento'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Complemento'),
        '#placeholder' => $this->t('digite aqui')
      ];

    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
    ];

     return $form;

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {


    /**
     * Iniciando variáveis
     */
    $count_errors = 0;
    $msg_erro = "";
    $msg_erro2 = "";
    $msg_erro3 = "";
    $msg_erro4 = "";
    $msg_erro5 = "";
    $msg_erro6 = "";
    $get_attributes = "";
    $user_existe = 0;

    /**
     * Inicio validação dos campos do formulário de criação de usuários
     */
    
    if($form_state->getValues()['genero'] == "0"){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Selecione um gênero.".$form_state->getValues()['genero']);

    }

    if($form_state->getValues()['endereco'] == ""){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Digite seu CEP.");

    }

    if($form_state->getValues()['cep'] == ""){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Digite seu CEP.");

    }

    if($form_state->getValues()['estado'] == 0){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Selecione um estado.");

    }

    if($form_state->getValues()['cidade'] == 0){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Selecione uma cidade.");

    }

    if($form_state->getValues()['bairro'] == ""){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Informe seu bairro.");

    }

    if($form_state->getValues()['numero'] == ""){
      $count_errors += 1;
      \Drupal::messenger()->addError("Erro: Digite o número.".$msg_erro6);
    }

  
  if($count_errors > 0){
    $count_errors = $count_errors;
  }else{
          try {

              $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                  'trace' => 1,
              ));
              
              // SET HEADERS
              $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
              $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
              $client->__setSoapHeaders($header);
              
              // Check if service is available
              $serviceStatus = $client->IsServiceAvailable();
              if ($serviceStatus != true) {
                  $message = 'Serviço indisponível';
                  return $message;
              }

              $codeuser = $_SESSION["get_user"]['codigo'];
              
              $data_atributos['atributos'] = [
                    [
                      'CodigoVisitante' => $codeuser,
                      'NomeAtributo' => 'id_sexo',
                      'Valor' => $form_state->getValues()['genero'],
                      'Items' => [
                        'Items' => [
                            "Id" => 5
                        ]
                      ]
                  ],
                  [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'nu_cep',
                        'Valor' => $form_state->getValues()['cep'],
                        'Items' => true
                    ],
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'dc_endereco',
                        'Valor' => $form_state->getValues()['endereco'],
                        'Items' => true
                    ],
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'dc_numero',
                        'Valor' => $form_state->getValues()['numero'],
                        'Items' => true
                    ],
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'dc_complemento',
                        'Valor' => $form_state->getValues()['complemento'],
                        'Items' => true
                    ],
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'dc_bairro',
                        'Valor' => $form_state->getValues()['bairro'],
                        'Items' => true
                    ],
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'dc_cidade',
                        'Valor' => $form_state->getValues()['cidade'],
                        'Items' => true
                    ],
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'sg_estado',
                        'Valor' => $form_state->getValues()['estado'],
                        'Items' => [
                          'Items' => [
                              "Id" => 32
                          ]
                        ]
                    ]
              ];
          
              $res = $client->SaveAttributes($data_atributos);
              $res_atributes = $res->SaveAttributesResult;
  
              \Drupal::messenger()->addMessage("Salvar atributo:".json_encode($res_atributes));

          } catch (SoapFault $exception) {
              $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
              \Drupal::messenger()->addError($response);
          }
                 
      
      $redirect_path = "/node/1";
      $url = url::fromUserInput($redirect_path);

      // set redirect
      $form_state->setRedirectUrl($url);
    
      

    }  

    return "Ok";

  }
}
