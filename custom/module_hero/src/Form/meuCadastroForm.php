<?php

namespace Drupal\module_hero\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class meuCadastroForm extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_ajaxhero";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Meu campos
     */

    $form['estado_civil'] = [
        '#type' => 'select',
        '#title' => $this
          ->t('Estado civil'),
        '#options' => [
            '0' => $this
            ->t('Selecionar'),
            '1' => $this
            ->t('Casado'),
            '2' => $this
            ->t('Divorciado'),
            '3' => $this
            ->t('Solteiro'),
            '4' => $this
            ->t('Viúvo'),
        ],
      ];


      $form['message'] = [
        '#type' => 'markup',
        '#markup' => '<div><p>Você tem criança na sua família? </br> Cadastre crianças para que a gente mostre conteúdos especiais paras as suas faixas de idade. Você não precisará fornecer os documentos do pequenos.</p></div>'
      ];

    

      $form['message2'] = [
        '#type' => 'markup',
        '#markup' => '<div><p>+ ADICIONANDO CRIANÇA</p></div>'
      ];

    /**
     * Campos criança 1
     */


    $form['nome_crianca_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nome completo *'),
    ];

    $form['datanascimento_crianca_1'] = array(
      '#type' => 'date',
      '#title' => $this->t('Data de nascimento *'),

    );

    $form['genero_crianca_1'] = [
        '#type' => 'select',
        '#title' => $this
          ->t('Gênero'),
        '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
        ],
      ];



    /**
     * Campos criança 2
     */


    $form['nome_crianca_2'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_2'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_2'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 3
     */


    $form['nome_crianca_3'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_3'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_3'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 4
     */


    $form['nome_crianca_4'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_4'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_4'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 5
     */


    $form['nome_crianca_5'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_5'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_5'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 6
     */


    $form['nome_crianca_6'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_6'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_6'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 7
     */


    $form['nome_crianca_7'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_7'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_7'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 8
     */


    $form['nome_crianca_8'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
    $form['datanascimento_crianca_8'] = array(
    '#type' => 'date',
    '#title' => $this->t('Data de nascimento *'),

    );

    $form['genero_crianca_8'] = [
        '#type' => 'select',
        '#title' => $this
        ->t('Gênero'),
        '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
        ],
    ];


        /**
     * Campos criança 9
     */


    $form['nome_crianca_9'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_9'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_9'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];


        /**
     * Campos criança 10
     */


    $form['nome_crianca_10'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Nome completo *'),
    ];
  
      $form['datanascimento_crianca_10'] = array(
        '#type' => 'date',
        '#title' => $this->t('Data de nascimento *'),
  
      );
  
      $form['genero_crianca_10'] = [
          '#type' => 'select',
          '#title' => $this
            ->t('Gênero'),
          '#options' => [
            'F' => $this
            ->t('Menina'),
            'M' => $this
            ->t('Menino')
          ],
        ];
  


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
    ];

     return $form;

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {


    /**
     * Iniciando variáveis
     */
    $count_errors = 0;
    $msg_erro = "";
    $msg_erroa = "";
    $msg_errob = "";
    $get_attributes = "";
    $user_existe = 0;


    /**
     * Inicio validação dos campos do formulário de criação de usuários
     */
    
      if($form_state->getValues()['estado_civil'] == 0){
        $count_errors += 1;
        $msg_erro = "Selecione um estado civil. \n";
      }

    
    if($count_errors > 0){
      \Drupal::messenger()->addError("Erro: ".$msg_erro);
    }else{
            try {

                $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                    'trace' => 1,
                ));
                
                // SET HEADERS
                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                $client->__setSoapHeaders($header);
                
                // Check if service is available
                $serviceStatus = $client->IsServiceAvailable();
                if ($serviceStatus != true) {
                    $message = 'Serviço indisponível';
                    return $message;
                }

                $codeuser = $_SESSION["get_user"]['codigo'];
                $estado_civil = $form_state->getValues()['estado_civil'];

                
                $data_atributos['atributos'] = [
                    [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'id_estado_civil',
                        'Valor' => $estado_civil,
                        'Items' => [
                            'Items' => [
                                "Id" => 8347
                            ]
                        ]
                    ]
                ];
            
                $res = $client->SaveAttributes($data_atributos);
                $res_atributes = $res->SaveAttributesResult;
    
                \Drupal::messenger()->addMessage("Salvar atributo:".json_encode($res_atributes));

            } catch (SoapFault $exception) {
                $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
                \Drupal::messenger()->addError($response);
            }
        
                $num_item = 0;
                
                for($x = 1; $x <= 10; $x++){

                    if($form_state->getValues()['nome_crianca_'.$x] != ""){
                        if($form_state->getValues()['datanascimento_crianca_'.$x] == "" || $form_state->getValues()['genero_crianca_'.$x] == ""){
                           
                            \Drupal::messenger()->addError("Preencha todos os campos para o filho número ".$x);
                            $x = 10;
                        }else{
                            try {

                                $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                                'trace' => 1,
                                ));
                            
                                // SET HEADERS
                                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                                $client->__setSoapHeaders($header);
                                
        
                                // Check if service is available
                                $serviceStatus = $client->IsServiceAvailable();
                                if ($serviceStatus != true) {
                                    $message = 'Serviço indisponível';
                                    return $message;
                                }
        
                                $codeuser = $_SESSION["get_user"]['codigo'];


                                if($x < 10){
                                    $num_item = "0".$x;
                                }else{
                                    $num_item = $x;
                                }
    
                                $data_atributos['atributos'] = [
                                    [
                                        'CodigoVisitante' => $codeuser,
                                        'NomeAtributo' => 'nm_filho_'.$num_item,
                                        'Valor' => $form_state->getValues()['nome_crianca_'.$x],
                                        'Items' => [
                                            'Items' => [
                                                "Id" => 8347
                                            ]
                                        ]
                                    ],[
                                        'CodigoVisitante' => $codeuser,
                                        'NomeAtributo' => 'dt_nascimento_filho_'.$num_item,
                                        'Valor' => date('d/m/Y', strtotime($form_state->getValues()['datanascimento_crianca_'.$x])),
                                        'Items' => true
                                    ],[
                                        'CodigoVisitante' => $codeuser,
                                        'NomeAtributo' => 'id_sexo_filho_'.$num_item,
                                        'Valor' => $form_state->getValues()['genero_crianca_'.$x],
                                        'Items' => [
                                           'Items' => [
                                              "Id" => 9751
                                           ]
                                        ]
                                    ]
                                ];

                                    
                                $res = $client->SaveAttributes($data_atributos);
                                $res_atributes = $res->SaveAttributesResult;
        
                                \Drupal::messenger()->addMessage("Salvar atributo:".json_encode($res_atributes));
    
                            } catch (SoapFault $exception) {
                                $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
                                \Drupal::messenger()->addError($response);
                            }
                        }
                    }            
                }
               
        
        $redirect_path = "/node/1";
        $url = url::fromUserInput($redirect_path);

        // set redirect
        $form_state->setRedirectUrl($url);
      

    }  

    return "Ok";

  }
}
