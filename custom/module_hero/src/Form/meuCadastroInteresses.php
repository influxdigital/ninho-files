<?php

namespace Drupal\module_hero\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class meuCadastroInteresses extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_ajaxhero";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Meu campos
     */


      $form['message'] = [
        '#type' => 'markup',
        '#markup' => '<div><p>Diga aqui quais são os tipos de conteúdo que lhe interessam:</p></div>'
      ];


   
      $form['message2'] = [
        '#type' => 'markup',
        '#markup' => "<div><p>SEUS INTERESSES ATUAIS:</p></div>"
      ];


      $form['message3'] = [
        '#type' => 'markup',
        '#markup' => "<div id='area_interesses'><p>ALGUMAS SUGESTÕES:</p></div>"
      ];

      $form['interesse_1'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Alimentação'),
      );


      $form['interesse_2'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Artes'),
      );

      $form['interesse_3'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Brincadeiras'),
      );

      $form['interesse_4'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Convivencia'),
      );

      $form['interesse_5'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Crescimento'),
      );

      $form['interesse_6'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Escola'),
      );


      $form['interesse_7'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Exercícios'),
      );

      $form['interesse_8'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Família'),
      );

      $form['interesse_9'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Leitura'),
      );

      $form['interesse_10'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Sono'),
      );

      $form['interesse_11'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Tecnologia'),
      );

      $form['interesse_12'] = array(
        '#type' => 'checkbox',
        '#title' => $this
          ->t('Verão'),
      );


      $form['titletag'] = [
        '#type' => 'markup',
        '#markup' => "<div><p>SUGIRA UMA TAG NOVA</p></div>"
      ];

      $form['sugestoestag'] = [
        '#type' => 'markup',
        '#markup' => "<div>SUGERIR</div>"
      ];

    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
    ];

     return $form;

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /**
     * Iniciando variáveis
     */
    $count_errors = 0;
    $count_interesses = 0;

    // Colocar aqui o numero de atributos existentes no Form.
    $num_atributos = 12;

    $msg_erro = "";
    $msg_erroa = "";
    $get_attributes = "";


    /**
     * Inicio validação dos campos do formulário de criação de usuários
     */
    
    for($x = 1; $x <= $num_atributos; $x++){
      if($form_state->getValues()['interesse_'.$x] == 1){
        $count_interesses += 1;
        $msg_erro = "Selecione um ou mais interesses. \n";
      }
    }
  
    if($count_interesses == 0){
      \Drupal::messenger()->addError("Erro: ".$msg_erro);
    }else{

              try {

                $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                  'trace' => 1,
                ));
              
                // SET HEADERS
                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                $client->__setSoapHeaders($header);
                
                // Check if service is available
                $serviceStatus = $client->IsServiceAvailable();
                if ($serviceStatus != true) {
                    $message = 'Serviço indisponível';
                    return $message;
                }
  
                $codeuser = $_SESSION["get_user"]['codigo'];
                
                for($x = 1; $x <= $num_atributos; $x++){
                  if($form_state->getValues()['interesse_1'] != 0){
                    $interesses[0] = 'Alimentação';
                  };

                  if($form_state->getValues()['interesse_2'] != 0){
                    $interesses[1] = 'Artes';
                  };

                  if($form_state->getValues()['interesse_3'] != 0){
                    $interesses[2] = 'Brincadeiras';
                  };

                  if($form_state->getValues()['interesse_4'] != 0){
                    $interesses[3] = 'Convivencia';
                  };


                  if($form_state->getValues()['interesse_5'] != 0){
                    $interesses[4] = 'Crescimento';
                  };


                  if($form_state->getValues()['interesse_6'] != 0){
                    $interesses[5] = 'Escola';
                  };

                  if($form_state->getValues()['interesse_7'] != 0){
                    $interesses[6] = 'Exercícios';
                  };

                  if($form_state->getValues()['interesse_8'] != 0){
                    $interesses[7] = 'Família';
                  };

                  if($form_state->getValues()['interesse_9'] != 0){
                    $interesses[8] = 'Leitura';
                  };

                  if($form_state->getValues()['interesse_10'] != 0){
                    $interesses[9] = 'Sono';
                  };

                  if($form_state->getValues()['interesse_11'] != 0){
                    $interesses[10] = 'Tecnologia';
                  };


                  if($form_state->getValues()['interesse_12'] != 0){
                    $interesses[11] = 'Verão';
                  };


                };
                

                $data_atributos['atributos'] = [
                  [
                    'CodigoVisitante' => $codeuser,
                    'NomeAtributo' => 'nm_tags',
                    'Valor' => implode(',',$interesses),
                    'Items' => true
                  ]
                ];

                $res = $client->SaveAttributes($data_atributos);
                $res_atributes = $res->SaveAttributesResult;
        
                  \Drupal::messenger()->addMessage("Seus interesses foram inseridos com sucesso!");
  
              } catch (SoapFault $exception) {
                $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
                \Drupal::messenger()->addError($response);
              }     


        $redirect_path = "/node/1";
        $url = url::fromUserInput($redirect_path);

        // set redirect
        $form_state->setRedirectUrl($url);
      

    }  

    return "Ok";

  }
}
