<?php

namespace Drupal\module_hero\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class updateUserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_ajaxhero";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Seu e-mail *'),
      '#value' => $this->t($_SESSION["get_user"]['email'])
    ];

    $form['nome'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu nome completo *'),
      '#placeholder' => $this->t($_SESSION["get_user"]['nome'])
    ];

    $form['telefone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu número de celular'),
      '#placeholder' => $this->t($_SESSION["get_attribute"]['valor_multivalorado_21'])
    ];

    $form['datanascimento'] = array(
      '#type' => 'date',
      '#title' => $this->t('Sua data de nascimento *'),
      '#value' => $this->t($_SESSION["get_user"]['data_nascimento'])

    );

    $form['senha_atual'] = [
      '#type' => 'password',
      '#title' => $this->t('Senha atual.')
    ];


    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Digite uma senha.')
    ];

    $form['senha2'] = [
      '#type' => 'password',
      '#title' => $this->t('Repita aqui a senha criada'),
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('ATUALIZAR'),
      '#button_type' => 'primary',
    ];

     return $form;

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /**
     * Campos de criação de atributos do novo usuário
     */
    $email =  $form_state->getValues()['email'];
    $telefone = $form_state->getValues()['telefone'];
    $datanascimento = $form_state->getValues()['datanascimento'];
    $nome = $form_state->getValues()['nome'];
    $senha_atual = $form_state->getValues()['senha_atual'];
    $senha_nova = $form_state->getValues()['senha2'];


    /**
     * Iniciando variáveis
     */
    $count_errors = 0;
    $msg_erro = "";
    $msg_erroa = "";
    $msg_errob = "";
    $get_attributes = "";
    $user_existe = 0;


    /**
     * Inicio validação dos campos do formulário de criação de usuários
     */
    
    if($form_state->getValues()['senha2'] != ""){
      if($form_state->getValues()['senha1'] != $form_state->getValues()['senha2']){
        $count_errors += 1;
        $msg_erro = "As senhas não coincidem \n";
      }else{
        $count_senha = 0;
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $form_state->getValues()['senha1']) )
          {
            $count_senha += 1;
          }
          
          if (preg_match("/^\d+$/", $form_state->getValues()['senha1'])) {
            $count_senha += 1;
          } 
  
          if(preg_match('/[A-Z]/', $form_state->getValues()['senha1'])){
              $count_senha += 1;
           }
  
           if(preg_match('/[a-z]/',  $form_state->getValues()['senha1'])){
              $count_senha += 1;
           }
  
           if($count_senha < 2){
            $count_errors += 1;
            $msg_erro = "Erro: A senha deve número, letras maiúsculas e minusculas e ter entre 6 a 10 caracteres.";
           }
      }
    }


    // if($form_state->getValues()['nome'] == "" || $form_state->getValues()['email'] == ""){
    //   $count_errors += 1;
    //   $msg_errob = "Preencha todos os campos obrigatórios! \n";
    // }

    /**
     * Fim do formulário de criação de usuários.
     */

    
    if($count_errors > 0){
      \Drupal::messenger()->addError("Erro: ".$msg_erro);
      \Drupal::messenger()->addError("Erro: ".$msg_erroa);
      \Drupal::messenger()->addError("Erro: ".$msg_errob);

    }else{

             // if($get_attributes->ValidationError->DescricaoErroCodigo == "6cdd91fc"){
            //   \Drupal::messenger()->addError("O e-mail já existe!");
            // }

            // if($get_attributes->ValidationError->NomeAtributo == "IdadeMinima"){
            //   \Drupal::messenger()->addError("O novo usuário deverá ser maior de 18 anos.");
            // }
          
            if($telefone != ""){
              try {

                $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                  'trace' => 1,
                ));
              
                // SET HEADERS
                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                $client->__setSoapHeaders($header);
                
                // Check if service is available
                $serviceStatus = $client->IsServiceAvailable();
                if ($serviceStatus != true) {
                    $message = 'Serviço indisponível';
                    return $message;
                }
  
                  $codeuser = $_SESSION["get_user"]['codigo'];
                  
                  $data_atributos['atributos'] = [
                      [
                        'CodigoVisitante' => $codeuser,
                        'NomeAtributo' => 'nu_celular',
                        'Valor' => $telefone,
                        'Items' => true
                    ],
                    [
                      'CodigoVisitante' => $codeuser,
                      'NomeAtributo' => 'fg_politica_privacidade',
                      'Valor' => 'true',
                      'Items' => true
                    ],
                    [
                      'CodigoVisitante' => $codeuser,
                      'NomeAtributo' => 'id_receber_newsletter',
                      'Valor' => 'S',
                      'Items' => [
                          'Items' => [
                            "Id" => 11
                          ]
                      ]
                    ],
                    [
                      'CodigoVisitante' => $codeuser,
                      'NomeAtributo' => 'id_receber_newsletter_email',
                      'Valor' => 'S',
                      'Items' => [
                          'Items' => [
                            "Id" => 11
                          ]
                      ]
                    ],
                    [
                      'CodigoVisitante' => $codeuser,
                      'NomeAtributo' => 'id_receber_newsletter_sms',
                      'Valor' => 'S',
                      'Items' => [
                          'Items' => [
                            "Id" => 11
                          ]
                      ]
                    ],
                    [
                      'CodigoVisitante' => $codeuser,
                      'NomeAtributo' => 'id_receber_newsletter_telefone',
                      'Valor' => 'S',
                      'Items' => [
                          'Items' => [
                            "Id" => 11
                          ]
                      ]
                    ]
                  ];
  
                          
                  /**
                   * Cria atributos do usuário
                   */
                
                  $res = $client->SaveAttributes($data_atributos);
                  $res_atributes = $res->SaveAttributesResult;
        
                  \Drupal::messenger()->addMessage("Salvar atributo:".json_encode($res_atributes));
  
              } catch (SoapFault $exception) {
                $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
                \Drupal::messenger()->addError($response);
              }
            }
            


            if($nome != ""){
              try {

                $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                  'trace' => 1,
                ));
                
                // SET HEADERS
                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                $client->__setSoapHeaders($header);
                
                // Check if service is available
                $serviceStatus = $client->IsServiceAvailable();
                if ($serviceStatus != true) {
                    $message = 'Serviço indisponível';
                    return $message;
                }
                  
                $res2 = "";
  
                $codeuser = $_SESSION["get_user"]['codigo'];
  
                $data_atributos2['user'] = [
                    'Codigo' => $codeuser,
                    'Nome' => $form_state->getValues()['nome'],
                    'dataNascimento' => '28171161863',
                    'Email' => $form_state->getValues()['email']
                ];
                        
                /**
                 * Cria atributos do usuário
                 */
                $res2 = $client->UpdateUser($data_atributos2);
                $res_atributes2 = $res2->UpdateUserResult;
  
                \Drupal::messenger()->addMessage("Nome:".json_encode($res_atributes2));
  
            } catch (SoapFault $exception) {
              $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
              \Drupal::messenger()->addError($response);
            }
            }
            



            try {

              $client = new SoapClient('https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl', array(
                'trace' => 1,
              ));
              
              // SET HEADERS
              $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
              $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
              $client->__setSoapHeaders($header);
              
              // Check if service is available
              $serviceStatus = $client->IsServiceAvailable();
              if ($serviceStatus != true) {
                  $message = 'Serviço indisponível';
                  return $message;
              }
                
              $res3 = "";

              $data_atributos3 = [
                  'username' => "user2001@teste.com",
                  'oldPassword' => $senha_atual,
                  'newPassword' =>  $senha_nova
              ];
                      
              /**
               * Cria atributos do usuário
               */
              $res3 = $client->ChangePassword($data_atributos3);
              $res_atributes3 = $res3->ChangePasswordResult;

              \Drupal::messenger()->addMessage("Trocar senha:".json_encode($res_atributes3));

          } catch (SoapFault $exception) {
            $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
            \Drupal::messenger()->addError($response);
          }


        $redirect_path = "/node/1";
        $url = url::fromUserInput($redirect_path);

        // set redirect
        $form_state->setRedirectUrl($url);
      

    }  

    return "Ok";

  }
}
