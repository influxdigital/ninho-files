<?php

namespace Drupal\module_nestle_drupal_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\module_nestle_drupal_integration\NdiArticleService;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Session\AccountProxy;

/**
 * This is our hero controller.
 */
class ndiController extends ControllerBase {

  private $articleNdiService;
  protected $configFactory;
  protected $currentUser;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_nestle_drupal_integration.hero_articles'),
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  public function __construct(NdiArticleService $articleNdiService, ConfigFactory $configFactory, AccountProxy $currentUser) {
    $this->articleNdiService = $articleBdiService;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
  }

  public function ndiList() {

    $heroes = [
      ['name' => 'Hulk'],
      ['name' => 'Thor'],
      ['name' => 'Iron Man'],
      ['name' => 'Luke Cage'],
      ['name' => 'Black Widow'],
      ['name' => 'Daredevil'],
      ['name' => 'Captain America'],
      ['name' => 'Wolverine']
    ];

    if ($this->currentUser->hasPermission('can see hero list')) {
      return [
        '#theme' => 'hero_list',
        '#items' => $heroes,
        '#title' => $this->configFactory->get('module_nestle_drupal_integration.settings')->get('hero_list_title'),
      ];
    }
    else {
      return [
        '#theme' => 'hero_list',
        '#items' => [],
        '#title' => $this->configFactory->get('module_nestle_drupal_integration.settings')->get('hero_list_title'),
      ];
    }



  }
}
