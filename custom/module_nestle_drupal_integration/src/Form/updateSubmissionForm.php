<?php

namespace Drupal\module_nestle_drupal_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Our custom hero form.
 */
class updateSubmissionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "module_hero_heroform";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['cep'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CEP'),
    ];

   $form['cidade'] = [
   '#type' => 'textfield',
   '#title' => $this->t('Cidade'),
   ];

   $form['estado'] = [
   '#type' => 'textfield',
   '#title' => $this->t('Estado'),
   ];


   $form['submit'] = [
     '#type' => 'button',
     '#value' => $this->t('Enviar'),
     
   ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $winner = rand(1, 2);
    drupal_set_message(
      'The winner between ' . $form_state->getValue('rival_1') . ' and ' .
      $form_state->getValue('rival_2') . ' is: ' . $form_state->getValue('rival_' . $winner)
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('rival_1'))) {
      $form_state->setErrorByName('rival_1', $this->t('Please specify rival one.'));
    }
  }

}
