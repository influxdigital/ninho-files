<?php

namespace Drupal\module_nestle_drupal_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Our example Form.
 */
class loginForm extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "module_hero_loginform";
   }

   /**
    * {@inheritdoc}
    */
    public function buildForm(array $form, FormStateInterface $form_state) {

      $form['usuario'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Usuário'),
      ];

    $form['senha'] = [
    '#type' => 'password',
    '#title' => $this->t('Senha'),
    ];

    $form['submit'] = [
        '#type' => 'button',
        '#value' => $this->t('Enviar'),
        
      ];

      $form['link_cadastro'] = [
        '#type' => 'url',
        '#title' => $this->t('Não possui login? Clique aqui para se cadastrar.'),
        // Example of a Url object from a route. See the documentation
        // for more methods that may help generate a Url object.
        // '#url' => Url::fromRoute('entity.node.canonical', ['node' => 81]),
      ];

      return $form;
    }

    /**
     * {@inheritdoc}
     */
     public function submitForm(array &$form, FormStateInterface $form_state) {
       drupal_set_message('Submitting our form...');
     }
}
